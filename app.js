'use strict';

const diet = require('diet');
const app = diet();

const ect = require('ect')({
  root: `${app.path}/views/html/`,
  watch: true,
  ext: '.html'
});
app.view('html', ect.render);

const stat = require('diet-static')({path: `${app.path}/views/`});
app.view('file', stat);

require('./route-handlers').mount(app);

app.get('/', ($) => {
  $.end();
});
exports = module.exports = app;