'use strict';

const app = require('./app');
const conf = require('config');
const port = process.env.PORT || conf.app.port;

app.listen(port);