'use strict';
const rp = require('request-promise-native'),
  mashape = require('config').get('app.mashape'),
  _ = require('lodash');

function callNutrionixApi($) {
  const options = {
    json: true,
    uri: `${mashape.url}${$.params.searchTerm}?fields=${mashape.fields}`,
    headers: {
      'X-Mashape-Key': mashape.headerValue
    }
  };
  rp(options)
    .then((response) => {
      const hits = response.hits;
      const result = [];
      _.forEach(hits, (item) => {
        let datum = {
          brandName: item.fields.brand_name,
          id: item.fields.item_id,
          name: item.fields.item_name,
          calories: item.fields.nf_calories,
          servingSizeQty: item.fields.nf_serving_size_qty,
          servingSizeUnit: item.fields.nf_serving_size_unit,
          totalFat: item.fields.nf_total_fat
        };
        result.push(datum);
      });
      $.data = _.reverse(_.sortBy(result, ['calories'] ));
      $.return();
    })
    .catch((error) => {
      throw error;
    });
}

function respondWithJson($) {
  $.json();
}

function mountApiRoutes(dietApp) {
  dietApp.get('/api/search/:searchTerm', callNutrionixApi, respondWithJson);
}

exports = module.exports.mount = mountApiRoutes;